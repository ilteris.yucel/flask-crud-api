## flask-crud-api

**python-pip** ile **requirements.txt**'yi kurarak, bağımlılıkları kurabilirsiniz.

**virtual enviroment** içine bağımlılıkları kurmak daha iyi bir çözümdür.

`cd [uygulamayı indirdiğiniz dosya konumu]` uygulama dizine gir.

`virtualenv <ad>` virtual enviroment oluştur.

`source ./venv/bin/activate` virtual enviroment'i çalıştır.

`pip install -r requirements.txt`  bağımlılıkları kur.

`python app.py` uygulamayı çalıştır.

**postman** üzerinde her bir **uri** ye başarılı

veya başarısız cevap dönmesini beklediğimiz istekler

atılmıştır. Server mantığımızın ve kontrollerimizin

beklediğimiz gibi çalışıp çalışmadığı test edilmiştir.

İstek sıralaması

1. Başarılı cevap alacak istek
2. 404 hatası alacak istek(Yanlış url)
3. 400 hatası alacak istek(Yanlış istek gövdesi)
4. Varsa diğer 400 hatası alacak istek

İstekler **POST** **PUT** **DELETE** işlemlerinin

sonucunu **/static/data.json**'a kaydeder. İstek

sonucunda 503 hatası alındıysa, /static/data.json dosyası

boşalmış veya silinmiş olabilir.

Tarayıcıdan görüntülemek için http://localhost:5000
