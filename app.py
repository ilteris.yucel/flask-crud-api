from flask import Flask, redirect, url_for, request, jsonify, abort, Response, make_response
import os, json, uuid
import util
from middleware import middleware


app = Flask(__name__)
app.wsgi_app = middleware(app.wsgi_app)

@app.route('/')
@app.route('/index')
def root():
  return 'INDEX'

@app.route('/get-all-questions', methods = ['GET'])
def get_all_questions():
  return make_response(jsonify(request.environ['data']), 200)

@app.route('/get-a-question/<id>', methods = ['GET'])
def get_a_question(id):
  data = request.environ['data']
  is_found = id in util.extract_id_list(data)
  quest = None

  if(not is_found):
    return make_response(jsonify({'message' : 'Given id is not found'}), 404)
  
  for q in data:
    if(q['id'] == id):
      quest = q
      break
  return make_response(jsonify(quest), 200)

@app.route('/add-question', methods=['POST'])
def add_question():
  key_list = ['title', 'problem', 'point', 'level', 'language', 'input', 'expected_output']
  request_data = request.get_json()
  data = request.environ['data']

  #gerekli tüm keylerin istekte sağlandığı kontrol edildi
  checker = all(field in request_data.keys() for field in key_list)

  #eğer tüm keyler sağlanmadıysa bad request verildi.
  if(not checker):
    return make_response(jsonify({'message' : 'Required fields are not found'}), 400)

  request_data['id'] = str(uuid.uuid4())
  data.append(request_data)
  with open(request.environ['filepath'], 'w') as df:
    json.dump(data, df, indent=4)

  return make_response(jsonify({'message' : 'Question is added'}), 200)

@app.route('/update-question-all-fields/<id>', methods=['PUT'])
def update_question_all_fields(id):
  key_list = ['title', 'problem', 'point', 'level', 'language', 'input', 'expected_output']  
  request_data = request.get_json()
  data = request.environ['data']
  is_found = False

  checker = all(field in request_data.keys() for field in key_list)

  if(not checker):
    return make_response(jsonify({'message' : 'Required fields are not found'}), 400)

  for q in data:
    if(q['id'] == id):
      is_found = True
      q.update(request_data)
      break

  if(not is_found):
    return make_response(jsonify({'message' : 'Given id is not found'}), 404)

  with open(request.environ['filepath'], 'w') as df:
    json.dump(data, df, indent=4)

  return make_response(jsonify({'message' : 'Question is updated'}), 200)

@app.route('/update-question-some-fields/<id>', methods=['PUT'])
def update_question_some_fields(id):
  key_list = ['title', 'problem', 'point', 'level', 'language', 'input', 'expected_output']  
  request_data = request.get_json()
  data = request.environ['data']
  is_found = False

  #istekte key_listesinde bulunmayan fieldlerin
  #bulunup bulunmadığını kontrol et
  checker = list(set(list(request_data.keys())) - set(key_list))

  if(len(checker)):
    return make_response(jsonify({'message' : 'Invalid fields'}), 400)

  for q in data:
    if(q['id'] == id):
      is_found = True
      q.update(request_data)
      break

  if(not is_found):
    return make_response(jsonify({'message' : 'Given id is not found'}), 404)

  with open(request.environ['filepath'], 'w') as df:
    json.dump(data, df, indent=4)

  return make_response(jsonify({'message' : 'Question is updated'}), 200)

@app.route('/update-question-one-field/<id>', methods=['PATCH'])
def update_question_one_fields(id):
  key_list = ['title', 'problem', 'point', 'level', 'language', 'input', 'expected_output']  
  request_data = request.get_json()
  data = request.environ['data']
  is_found = False
  #Güncellenecek field'i url parametresi olarak al
  field = request.args.get('field')
  checker = field in key_list

  if(not checker):
    return make_response(jsonify({'message' : 'Invalid field'}), 400)

  for q in data:
    if(q['id'] == id):
      is_found = True
      q[field] = request_data['data']
      break

  if(not is_found):
    return make_response(jsonify({'message' : 'Given id is not found'}), 404)

  with open(request.environ['filepath'], 'w') as df:
    json.dump(data, df, indent=4)

  return make_response(jsonify({'message' : 'Question field is updated'}), 200)

@app.route('/delete-question/<id>', methods=['DELETE'])
def delete_question(id):
  data = request.environ['data']
  is_found = False

  for q in data:
    if(q['id'] == id):
      is_found = True
      data.remove(q)
      break
  if(not is_found):
    return make_response(jsonify({'message' : 'Given id is not found'}), 404)

  with open(request.environ['filepath'], 'w') as df:
    json.dump(data, df, indent=4)

  return make_response(jsonify({'message' : 'Question is deleted'}), 200)

@app.route('/delete-multiple-questions', methods=['POST'])
def delete_multiple_questions():
  request_data = request.get_json()
  data = request.environ['data']

  checker = all(id in util.extract_id_list(data) for id in request_data)

  if(not checker):
    return make_response(jsonify({'message' : 'Some ids is not found'}), 400)

  data = [quest for quest in data if not quest["id"] in request_data]
  print(data)

  with open(request.environ['filepath'], 'w') as df:
    json.dump(data, df, indent=4)

  return make_response(jsonify({'message' : 'Questions are deleted'}), 200)

@app.route('/search-with-field/<field>', methods=['GET'])
def search_with_field(field):
  key_list = ['title', 'problem', 'point', 'level', 'language', 'input', 'expected_output']  
  kw = request.args.get('kw')
  checker = field in key_list
  if(not checker):
    return make_response(jsonify({'message' : 'Field is not found'}), 404)
  data = [data for data in request.environ['data'] if kw in data[field]]  

  return make_response(jsonify(data), 200)

if __name__ == '__main__':
  app.run(debug = True)