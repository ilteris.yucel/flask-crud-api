from werkzeug.wrappers import Request, Response
import os, json

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

class middleware():

  def __init__(self, app):
    self.app = app
    self.token = 'test'
    self.data_file_path = os.path.join(SCRIPT_DIR, 'static', 'data.json')

  def __call__(self, environ, start_response):
    request = Request(environ)
    #Hardcoded token için
    #url parametresi kullanıldı.
    token = request.args.get('token')
        
    if token != self.token:
      res = Response(u'Authorization failed', mimetype= 'text/plain', status=401)
      return res(environ, start_response)

    if(not os.path.exists(self.data_file_path)):
      res = Response(u'Cannot load static file', mimetype= 'text/plain', status=503)
      return res(environ, start_response)
    #Normalde middleware'de data okumak mantıksız olabilir.
    #Farklı url'ler, farklı datalar kullanabilir.
    #Ancak bu proje özelinde url'ler aynı datayı okuduğundan dolayı
    #middleware'de okumamız kod tekrarını azalttı. 
    questions_data = None
    with open(self.data_file_path) as df:
      raw_data = df.read()
      questions_data = json.loads(raw_data)
    if(not questions_data):
      res = Response(u'Cannot read static file', mimetype= 'text/plain', status=503)
      return res(environ, start_response) 

    environ['token'] = token
    environ['filepath'] = self.data_file_path
    environ['data'] = questions_data
    return self.app(environ, start_response)


